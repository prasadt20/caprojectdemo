package com.abc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

public class SampleController extends AbstractController {
	
	public ModelAndView handleRequestInternal(HttpServletRequest request,HttpServletResponse response) throws Exception {
			ModelAndView mav = new ModelAndView("success");
			mav.addObject("message", "Hello World!");
			return mav;
	}
}
	
		
