package com.abc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.abc.bean.UserDetails;
import com.abc.service.ProfileService;

@Controller
public class UserDetailsController {
	
	@Autowired
	private ProfileService profileService;
	
	@RequestMapping("/regform")
	public String getForm() {
		return "regform";		
	}
	
	@RequestMapping("/searchform")
	public String searchForm() {
		return "searchform";		
	}
	

	@RequestMapping(value="/register", method=RequestMethod.POST)
	public String saveUser(@ModelAttribute UserDetails userDetails, ModelMap map) {
		int result = profileService.saveProfile(userDetails);
		map.addAttribute("userDetails", result);
		return "success";		
	}
	
	@RequestMapping(value="/find", method=RequestMethod.POST)
	public String findUserByUsername(@RequestParam("username") String username, ModelMap map) {
		UserDetails userDetails = profileService.searchByUsername(username);
		map.addAttribute("userDetails", userDetails);
		return "searchform";		
	}
}
