package com.abc.dao;

import com.abc.bean.UserDetails;

public interface ProfileDAO {
	
	int createProfile(UserDetails userDetails);
	UserDetails getUserByUserName(String username);

}
