package com.abc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
	
	@RequestMapping("/about")
	public String aboutAspire(ModelMap map) {
//		String message = "Hello from @Aspire Info labs";
//		map.addAttribute("message", message);
		return "about";
	}
}
