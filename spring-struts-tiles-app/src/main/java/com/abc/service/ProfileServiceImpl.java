package com.abc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abc.bean.UserDetails;
import com.abc.dao.ProfileDAO;

@Service
@Transactional
public class ProfileServiceImpl implements ProfileService {
	
	@Autowired
	private ProfileDAO profileDAO;
	
	public void setProfileDAO(ProfileDAO profileDAO) {
		this.profileDAO = profileDAO;
	}
	
	@Override
	public int saveProfile(UserDetails userDetails) {
	
		return profileDAO.createProfile(userDetails);
	}

	@Override
	public UserDetails searchByUsername(String username) {
	
		return profileDAO.getUserByUserName(username);
	}

}
