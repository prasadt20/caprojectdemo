package com.abc.service;

import com.abc.bean.UserDetails;

public interface ProfileService {

	int saveProfile(UserDetails userDetails);
	UserDetails searchByUsername(String username);
}
